﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="controls" Type="Folder">
			<Item Name="analyse" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="AggregatedAnalysisData.ctl" Type="VI" URL="../AggregatedAnalysisData.ctl"/>
				<Item Name="AnalyseState.ctl" Type="VI" URL="../AnalyseState.ctl"/>
			</Item>
			<Item Name="unsorted" Type="Folder">
				<Item Name="ConfigButton.ctl" Type="VI" URL="../ConfigButton.ctl"/>
				<Item Name="CANCELbutton.ctl" Type="VI" URL="../CANCELbutton.ctl"/>
				<Item Name="OKbutton-new.ctl" Type="VI" URL="../OKbutton-new.ctl"/>
				<Item Name="CalibrateButtonBIG.ctl" Type="VI" URL="../CalibrateButtonBIG.ctl"/>
				<Item Name="CustomAnalyseButtonSmall.ctl" Type="VI" URL="../CustomAnalyseButtonSmall.ctl"/>
				<Item Name="CustomSliderOpenCloseButton.ctl" Type="VI" URL="../CustomSliderOpenCloseButton.ctl"/>
				<Item Name="ProductEnum.ctl" Type="VI" URL="../ProductEnum.ctl"/>
				<Item Name="CustomCloseButtonLong.ctl" Type="VI" URL="../CustomCloseButtonLong.ctl"/>
				<Item Name="GreenButton.ctl" Type="VI" URL="../GreenButton.ctl"/>
				<Item Name="GrayButton.ctl" Type="VI" URL="../GrayButton.ctl"/>
				<Item Name="ProductEnumNew.ctl" Type="VI" URL="../ProductEnumNew.ctl"/>
				<Item Name="ProductEnumTypeDef.ctl" Type="VI" URL="../ProductEnumTypeDef.ctl"/>
			</Item>
			<Item Name="measurement" Type="Folder">
				<Item Name="MeasurementOutputCluster.ctl" Type="VI" URL="../MeasurementOutputCluster.ctl"/>
				<Item Name="AggregatedMeasurementSmall.ctl" Type="VI" URL="../AggregatedMeasurementSmall.ctl"/>
				<Item Name="AggregatedMeasurementEvaluation.ctl" Type="VI" URL="../AggregatedMeasurementEvaluation.ctl"/>
				<Item Name="MeasurementStates.ctl" Type="VI" URL="../MeasurementStates.ctl"/>
				<Item Name="MeasurementEvalState.ctl" Type="VI" URL="../MeasurementEvalState.ctl"/>
			</Item>
			<Item Name="common" Type="Folder">
				<Item Name="SystemConfiguration.ctl" Type="VI" URL="../SystemConfiguration.ctl"/>
				<Item Name="ExecutionStates.ctl" Type="VI" URL="../ExecutionStates.ctl"/>
				<Item Name="CustomChannelInput.ctl" Type="VI" URL="../CustomChannelInput.ctl"/>
				<Item Name="channel configuration short.ctl" Type="VI" URL="../channel configuration short.ctl"/>
			</Item>
			<Item Name="settings" Type="Folder">
				<Item Name="SettingActions.ctl" Type="VI" URL="../SettingActions.ctl"/>
				<Item Name="ProductConfigStates.ctl" Type="VI" URL="../ProductConfigStates.ctl"/>
				<Item Name="ProductConfigurationActions.ctl" Type="VI" URL="../ProductConfigurationActions.ctl"/>
			</Item>
			<Item Name="ui" Type="Folder">
				<Item Name="CustomClearDateIndicator.ctl" Type="VI" URL="../CustomClearDateIndicator.ctl"/>
				<Item Name="ApplyButton.ctl" Type="VI" URL="../ApplyButton.ctl"/>
				<Item Name="AnalyseDateFilteredDataButton.ctl" Type="VI" URL="../AnalyseDateFilteredDataButton.ctl"/>
				<Item Name="WorkerAccessButton.ctl" Type="VI" URL="../WorkerAccessButton.ctl"/>
				<Item Name="SaveButton.ctl" Type="VI" URL="../SaveButton.ctl"/>
				<Item Name="ReloadButton.ctl" Type="VI" URL="../ReloadButton.ctl"/>
				<Item Name="OKsmallButton.ctl" Type="VI" URL="../OKsmallButton.ctl"/>
				<Item Name="OKbutton.ctl" Type="VI" URL="../OKbutton.ctl"/>
				<Item Name="CustomScaleSlider.ctl" Type="VI" URL="../CustomScaleSlider.ctl"/>
				<Item Name="CustomMeasurementBigButton.ctl" Type="VI" URL="../CustomMeasurementBigButton.ctl"/>
				<Item Name="CustomMeasurementButton.ctl" Type="VI" URL="../CustomMeasurementButton.ctl"/>
				<Item Name="CustomCloseButton.ctl" Type="VI" URL="../CustomCloseButton.ctl"/>
				<Item Name="CustomConfigButton.ctl" Type="VI" URL="../CustomConfigButton.ctl"/>
				<Item Name="CustomAnalyseBigButton.ctl" Type="VI" URL="../CustomAnalyseBigButton.ctl"/>
				<Item Name="CustomAnalyseButton.ctl" Type="VI" URL="../CustomAnalyseButton.ctl"/>
				<Item Name="CustomCalendar.ctl" Type="VI" URL="../CustomCalendar.ctl"/>
				<Item Name="CustomCalendarIconButton.ctl" Type="VI" URL="../CustomCalendarIconButton.ctl"/>
				<Item Name="CalibrateButton.ctl" Type="VI" URL="../CalibrateButton.ctl"/>
				<Item Name="AdminLoginButton.ctl" Type="VI" URL="../AdminLoginButton.ctl"/>
				<Item Name="AnalyseAllDataButton.ctl" Type="VI" URL="../AnalyseAllDataButton.ctl"/>
				<Item Name="AnalyseNumberFilteredDataButton.ctl" Type="VI" URL="../AnalyseNumberFilteredDataButton.ctl"/>
			</Item>
		</Item>
		<Item Name="dialogs" Type="Folder">
			<Item Name="unused" Type="Folder">
				<Item Name="ChannelconfigPopUp.vi" Type="VI" URL="../ChannelconfigPopUp.vi"/>
			</Item>
			<Item Name="AnalyseDialog.vi" Type="VI" URL="../AnalyseDialog.vi"/>
			<Item Name="PasswordPopUp.vi" Type="VI" URL="../PasswordPopUp.vi"/>
			<Item Name="MeasureDialog.vi" Type="VI" URL="../MeasureDialog.vi"/>
			<Item Name="ProductConfigurationDialog.vi" Type="VI" URL="../ProductConfigurationDialog.vi"/>
			<Item Name="MeasureDialogRefactored.vi" Type="VI" URL="../MeasureDialogRefactored.vi"/>
			<Item Name="PasswordPopUpSimple.vi" Type="VI" URL="../PasswordPopUpSimple.vi"/>
			<Item Name="ProductConfigurationDialog-Master.vi" Type="VI" URL="../ProductConfigurationDialog-Master.vi"/>
			<Item Name="ProductÄndernDialog.vi" Type="VI" URL="../ProductÄndernDialog.vi"/>
		</Item>
		<Item Name="FGV" Type="Folder">
			<Item Name="ProductconfigFGV.vi" Type="VI" URL="../ProductconfigFGV.vi"/>
			<Item Name="ProductSettingsFGV.vi" Type="VI" URL="../ProductSettingsFGV.vi"/>
			<Item Name="SystemConfigFGV.vi" Type="VI" URL="../SystemConfigFGV.vi"/>
		</Item>
		<Item Name="functions" Type="Folder">
			<Item Name="unsorted" Type="Folder">
				<Item Name="ReadConfigData-func.vi" Type="VI" URL="../ReadConfigData-func.vi"/>
				<Item Name="GetIniFilePath.vi" Type="VI" URL="../GetIniFilePath.vi"/>
				<Item Name="GetSystemConfigFilePath.vi" Type="VI" URL="../GetSystemConfigFilePath.vi"/>
				<Item Name="GetProductConfigPath.vi" Type="VI" URL="../GetProductConfigPath.vi"/>
				<Item Name="GetMeasurmentFilePath.vi" Type="VI" URL="../GetMeasurmentFilePath.vi"/>
				<Item Name="LoadModuleInformation-func.vi" Type="VI" URL="../LoadModuleInformation-func.vi"/>
				<Item Name="LoadModulesConfiguration.vi" Type="VI" URL="../LoadModulesConfiguration.vi"/>
				<Item Name="ImportConfigFile-func.vi" Type="VI" URL="../ImportConfigFile-func.vi"/>
				<Item Name="ExportConfigFile2-func.vi" Type="VI" URL="../ExportConfigFile2-func.vi"/>
				<Item Name="MoveSlider.vi" Type="VI" URL="../MoveSlider.vi"/>
				<Item Name="Position Frame Center.vi" Type="VI" URL="../Position Frame Center.vi"/>
			</Item>
			<Item Name="measurement" Type="Folder">
				<Item Name="CalculateChannelMeasurement-func.vi" Type="VI" URL="../CalculateChannelMeasurement-func.vi"/>
				<Item Name="CalculateMeasurementColor-func.vi" Type="VI" URL="../CalculateMeasurementColor-func.vi"/>
				<Item Name="UpperLowerThreshold-func.vi" Type="VI" URL="../UpperLowerThreshold-func.vi"/>
				<Item Name="OutOfOrEqualBorder-func.vi" Type="VI" URL="../OutOfOrEqualBorder-func.vi"/>
				<Item Name="ConvertChannelMeasurmentToSimple-func.vi" Type="VI" URL="../ConvertChannelMeasurmentToSimple-func.vi"/>
				<Item Name="convertChannelMeasurementClusterToArray-func.vi" Type="VI" URL="../convertChannelMeasurementClusterToArray-func.vi"/>
				<Item Name="convertAggregatedMeasurementClusterToArray-func.vi" Type="VI" URL="../convertAggregatedMeasurementClusterToArray-func.vi"/>
				<Item Name="SaveMeasurementToCSV.vi" Type="VI" URL="../SaveMeasurementToCSV.vi"/>
				<Item Name="latch-data-func.vi" Type="VI" URL="../latch-data-func.vi"/>
				<Item Name="EvaluateMeasurementSingleIteration2-func.vi" Type="VI" URL="../EvaluateMeasurementSingleIteration2-func.vi"/>
			</Item>
			<Item Name="analyse" Type="Folder">
				<Item Name="ReadFromSimpleFileToAnalyse.vi" Type="VI" URL="../ReadFromSimpleFileToAnalyse.vi"/>
				<Item Name="convertArrayToChannelMeasurementCluster-func.vi" Type="VI" URL="../convertArrayToChannelMeasurementCluster-func.vi"/>
				<Item Name="praparePlotData.vi" Type="VI" URL="../praparePlotData.vi"/>
				<Item Name="CountDailyMeasurements.vi" Type="VI" URL="../CountDailyMeasurements.vi"/>
				<Item Name="ClearTimeStamp.vi" Type="VI" URL="../ClearTimeStamp.vi"/>
				<Item Name="CountMeasurments.vi" Type="VI" URL="../CountMeasurments.vi"/>
				<Item Name="FilterByTime.vi" Type="VI" URL="../FilterByTime.vi"/>
				<Item Name="calculateOverallStatistics.vi" Type="VI" URL="../calculateOverallStatistics.vi"/>
			</Item>
		</Item>
		<Item Name="measurements" Type="Folder">
			<Item Name="P1.csv" Type="Document" URL="../../builds/Final/measurements/P1.csv"/>
			<Item Name="P2.csv" Type="Document" URL="../../builds/Final/measurements/P2.csv"/>
			<Item Name="P3.csv" Type="Document" URL="../../builds/Final/measurements/P3.csv"/>
		</Item>
		<Item Name="config" Type="Folder">
			<Item Name="dqc-settings.ini" Type="Document" URL="../../config/dqc-settings.ini"/>
			<Item Name="ModuleConfig.xml" Type="Document" URL="../../builds/Final/config/ModuleConfig.xml"/>
			<Item Name="P1.csv" Type="Document" URL="/C/DigitalePruefstation/Input/P1.csv"/>
			<Item Name="P2.csv" Type="Document" URL="/C/DigitalePruefstation/Input/P2.csv"/>
			<Item Name="P3.csv" Type="Document" URL="/C/DigitalePruefstation/Input/P3.csv"/>
		</Item>
		<Item Name="MSELibraries" Type="Folder">
			<Item Name="MSElibrary.dll" Type="Document" URL="/C/Program Files (x86)/HEIDENHAIN/MSElibrary/MSElibrary.dll"/>
			<Item Name="QtCore4.dll" Type="Document" URL="/C/Program Files (x86)/HEIDENHAIN/MSElibrary/QtCore4.dll"/>
			<Item Name="QtXml4.dll" Type="Document" URL="/C/Program Files (x86)/HEIDENHAIN/MSElibrary/QtXml4.dll"/>
		</Item>
		<Item Name="Digitale Pruefstation.vi" Type="VI" URL="../Digitale Pruefstation.vi"/>
		<Item Name="logo256_TXU_icon.ico" Type="Document" URL="/C/Users/User/Downloads/logo256_TXU_icon.ico"/>
		<Item Name="logo_qzE_icon.ico" Type="Document" URL="/C/Users/User/Downloads/logo_qzE_icon.ico"/>
		<Item Name="Analyse2Point0.vi" Type="VI" URL="../../../../../../1_Devel/LabView/CustomControls/Analyse2Point0.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
			</Item>
			<Item Name="MSElibrary1VppWrapperVIs.lvlib" Type="Library" URL="/C/Users/User/Documents/HEIDENHAIN/MSElibrary/LabVIEW/Wrappers/Mse1VppModule/MSElibrary1VppWrapperVIs.lvlib"/>
			<Item Name="MSElibraryConfigFileWrapperVIs.lvlib" Type="Library" URL="/C/Users/User/Documents/HEIDENHAIN/MSElibrary/LabVIEW/Wrappers/MseConfigFile/MSElibraryConfigFileWrapperVIs.lvlib"/>
			<Item Name="MSElibraryEndatWrapperVIs.lvlib" Type="Library" URL="/C/Users/User/Documents/HEIDENHAIN/MSElibrary/LabVIEW/Wrappers/MseEndatModule/MSElibraryEndatWrapperVIs.lvlib"/>
			<Item Name="MSElibraryModuleWrapperVIs.lvlib" Type="Library" URL="/C/Users/User/Documents/HEIDENHAIN/MSElibrary/LabVIEW/Wrappers/MseModule/MSElibraryModuleWrapperVIs.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="DPApp_2015.05.11" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{8176DAEE-4AC7-4EFE-A408-B2CB0CBAAD9C}</Property>
				<Property Name="App_INI_GUID" Type="Str">{3D1A68FA-357D-4983-8147-8BBDD7DB13B6}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{0BBDBEBB-477A-46E8-8B3B-A350FB61DD83}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DPApp_2015.05.11</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">German</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/DBApp_2015.05</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{7FC70D52-7EC8-4999-B631-19183E5BEE3E}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">German</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.build" Type="Int">27</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DPApp.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/DBApp_2015.05/DPApp_2015.05.11.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/DBApp_2015.05/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].destName" Type="Str">config</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/DBApp_2015.05/config</Property>
				<Property Name="Destination[2].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[2].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[3].destName" Type="Str">measurements</Property>
				<Property Name="Destination[3].path" Type="Path">../builds/DBApp_2015.05/measurements</Property>
				<Property Name="Destination[3].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/logo_qzE_icon.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{23CF0069-06CC-49FA-9DD1-4BA25EC003D8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Digitale Pruefstation.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/measurements</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/config</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
				<Property Name="TgtF_companyName" Type="Str">Nachhaltige Industrielösungen GmbHs</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DPApp_2015.05.11</Property>
				<Property Name="TgtF_internalName" Type="Str">DPApp_2015.05.11</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 </Property>
				<Property Name="TgtF_productName" Type="Str">DPApp_2015.05.11</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{8DB0D8B3-AF87-4449-9BCA-596560BCE9C0}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DPApp.exe</Property>
			</Item>
			<Item Name="Digitalle Messstation Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">DPApp</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{F0E78A72-D8BE-4E85-84BE-ECA2FC4E0FF2}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">DPApp</Property>
				<Property Name="Destination[1].parent" Type="Str">{624309A2-B0CB-4149-B964-A0FF8B968B6A}</Property>
				<Property Name="Destination[1].tag" Type="Str">{EEF40A2F-C07D-46FA-B612-3D3FB47200C3}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{1601A0F4-B5D1-4488-881C-9158DFB0D05C}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2014 f1</Property>
				<Property Name="DistPart[0].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2014 Non-English Support.</Property>
				<Property Name="DistPart[0].SoftDep[0].upgradeCode" Type="Str">{CAC8FA79-6D3D-4263-BB7B-1A706EF87C08}</Property>
				<Property Name="DistPart[0].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[0].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[0].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[10].productName" Type="Str">NI mDNS Responder 14.0</Property>
				<Property Name="DistPart[0].SoftDep[10].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[0].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[11].productName" Type="Str">NI Deployment Framework 2014</Property>
				<Property Name="DistPart[0].SoftDep[11].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[0].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[12].productName" Type="Str">NI Error Reporting 2014</Property>
				<Property Name="DistPart[0].SoftDep[12].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[0].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[2].productName" Type="Str">NI Service Locator 14.0</Property>
				<Property Name="DistPart[0].SoftDep[2].upgradeCode" Type="Str">{B235B862-6A92-4A04-A8B2-6D71F777DE67}</Property>
				<Property Name="DistPart[0].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[3].productName" Type="Str">Math Kernel Libraries</Property>
				<Property Name="DistPart[0].SoftDep[3].upgradeCode" Type="Str">{3BDD0408-2F90-4B42-9777-5ED1D4BE67A8}</Property>
				<Property Name="DistPart[0].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[4].productName" Type="Str">NI Logos 14.0</Property>
				<Property Name="DistPart[0].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[0].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[5].productName" Type="Str">NI TDM Streaming 14.0</Property>
				<Property Name="DistPart[0].SoftDep[5].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[0].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2014</Property>
				<Property Name="DistPart[0].SoftDep[6].upgradeCode" Type="Str">{4A8BDBBB-DA1C-45C9-8563-74C034FBD357}</Property>
				<Property Name="DistPart[0].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[7].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2014</Property>
				<Property Name="DistPart[0].SoftDep[7].upgradeCode" Type="Str">{4372F3E3-5935-4012-93AB-B6710CE24920}</Property>
				<Property Name="DistPart[0].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[8].productName" Type="Str">NI VC2008MSMs</Property>
				<Property Name="DistPart[0].SoftDep[8].upgradeCode" Type="Str">{FDA3F8BB-BAA9-45D7-8DC7-22E1F5C76315}</Property>
				<Property Name="DistPart[0].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[9].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[0].SoftDep[9].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[0].SoftDepCount" Type="Int">13</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{4722F14B-8434-468D-840D-2B0CD8CBD5EA}</Property>
				<Property Name="DistPartCount" Type="Int">1</Property>
				<Property Name="INST_buildLocation" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/DPAppInstaller</Property>
				<Property Name="INST_buildSpecName" Type="Str">Digitalle Messstation Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{EEF40A2F-C07D-46FA-B612-3D3FB47200C3}</Property>
				<Property Name="INST_productName" Type="Str">Digitale Messstation</Property>
				<Property Name="INST_productVersion" Type="Str">1.6.7</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14008036</Property>
				<Property Name="MSI_arpCompany" Type="Str">NIL Nachhaltige Industrielösungen GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">info@nil-gmbh.de</Property>
				<Property Name="MSI_arpURL" Type="Str">www.nil-gmbh.de</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{E339E884-9493-4E62-99E9-AA8D1F431AEC}</Property>
				<Property Name="MSI_osCheck" Type="Int">5</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{2EB0E160-F3FA-4F43-9B6B-030390146A4F}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{F0E78A72-D8BE-4E85-84BE-ECA2FC4E0FF2}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{F0E78A72-D8BE-4E85-84BE-ECA2FC4E0FF2}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">DPApp.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">2</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">DPApp</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Digitale Messstation</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{22BE6095-5139-4022-9A89-0B15AC635712}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">DPApp_2015.05.12</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/DPApp_2015.05.12</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="DPApp_2015.05.12" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9758E234-8489-4415-BAE2-F6778FEACA1B}</Property>
				<Property Name="App_INI_GUID" Type="Str">{DD10C2DE-1180-42E5-9092-CF3CDB4F10B7}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{B6B5E735-DC8B-47BB-8EBF-318602E39325}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DPApp_2015.05.12</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">German</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{71C80385-C59C-4254-BFA7-EAA3B6F05B36}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">German</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.build" Type="Int">33</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DPApp.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App/DPApp_2015.05.12.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">config</Property>
				<Property Name="Destination[2].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App/config</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">measurements</Property>
				<Property Name="Destination[3].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App/measurements</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/logo_qzE_icon.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{CF651B0A-5DD8-4D7F-BFE6-EB0C5F92BCC1}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Digitale Pruefstation.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/measurements</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/config</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
				<Property Name="TgtF_companyName" Type="Str">Nachhaltige Industrielösungen GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DPApp_2015.05.12</Property>
				<Property Name="TgtF_internalName" Type="Str">DPApp_2015.05.12</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 </Property>
				<Property Name="TgtF_productName" Type="Str">DPApp_2015.05.11</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{22BE6095-5139-4022-9A89-0B15AC635712}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DPApp.exe</Property>
			</Item>
			<Item Name="DPApp_2015.05.28" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{27D838F5-8CEB-4C04-8611-FD9474589C1E}</Property>
				<Property Name="App_INI_GUID" Type="Str">{36B4AEFC-7428-443E-9D5E-D7FF8B4AF911}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{7634C519-86CB-42DC-AF51-881A5636A81A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DPApp_2015.05.28</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">German</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App2015.05.28</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{DF9CCC47-3591-4EE9-8A21-3ECF2A08D4DB}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">German</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.build" Type="Int">37</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DPApp.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App2015.05.28/DPApp_2015.05.28.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App2015.05.28/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">config</Property>
				<Property Name="Destination[2].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App2015.05.28/config</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">measurements</Property>
				<Property Name="Destination[3].path" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/App2015.05.28/measurements</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/logo_qzE_icon.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{8869BF6B-3DC0-4DEE-A2B4-36822ECA3750}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Digitale Pruefstation.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/measurements</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/config</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/MSELibraries</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
				<Property Name="TgtF_companyName" Type="Str">Nachhaltige Industrielösungen GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DPApp_2015.05.12</Property>
				<Property Name="TgtF_internalName" Type="Str">DPApp_2015.05.12</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 </Property>
				<Property Name="TgtF_productName" Type="Str">DPApp_2015.05.11</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{4490496D-7397-48E8-879F-46E560F8BE81}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DPApp.exe</Property>
			</Item>
			<Item Name="DPApp Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">HEIDENHAIN</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{3C58C0EF-E403-4648-A16D-DAF414B62750}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">MSElibrary</Property>
				<Property Name="Destination[1].parent" Type="Str">{3C58C0EF-E403-4648-A16D-DAF414B62750}</Property>
				<Property Name="Destination[1].tag" Type="Str">{F9C6324F-5685-4112-B8BD-C90C4DF2F2A1}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="Destination[2].name" Type="Str">DPApp</Property>
				<Property Name="Destination[2].parent" Type="Str">{C63B6F86-C439-4240-9AAE-EC6A9DDD0A29}</Property>
				<Property Name="Destination[2].tag" Type="Str">{88F66E09-4DFF-4818-8248-5EEFDD23F098}</Property>
				<Property Name="Destination[2].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{1601A0F4-B5D1-4488-881C-9158DFB0D05C}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2014 f1</Property>
				<Property Name="DistPart[0].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[0].productName" Type="Str">NI LabVIEW Run-Time Engine 2014 Non-English Support.</Property>
				<Property Name="DistPart[0].SoftDep[0].upgradeCode" Type="Str">{CAC8FA79-6D3D-4263-BB7B-1A706EF87C08}</Property>
				<Property Name="DistPart[0].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[0].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[0].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[10].productName" Type="Str">NI mDNS Responder 14.0</Property>
				<Property Name="DistPart[0].SoftDep[10].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[0].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[11].productName" Type="Str">NI Deployment Framework 2014</Property>
				<Property Name="DistPart[0].SoftDep[11].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[0].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[12].productName" Type="Str">NI Error Reporting 2014</Property>
				<Property Name="DistPart[0].SoftDep[12].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[0].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[2].productName" Type="Str">NI Service Locator 14.0</Property>
				<Property Name="DistPart[0].SoftDep[2].upgradeCode" Type="Str">{B235B862-6A92-4A04-A8B2-6D71F777DE67}</Property>
				<Property Name="DistPart[0].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[3].productName" Type="Str">Math Kernel Libraries</Property>
				<Property Name="DistPart[0].SoftDep[3].upgradeCode" Type="Str">{3BDD0408-2F90-4B42-9777-5ED1D4BE67A8}</Property>
				<Property Name="DistPart[0].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[4].productName" Type="Str">NI Logos 14.0</Property>
				<Property Name="DistPart[0].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[0].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[5].productName" Type="Str">NI TDM Streaming 14.0</Property>
				<Property Name="DistPart[0].SoftDep[5].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[0].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2014</Property>
				<Property Name="DistPart[0].SoftDep[6].upgradeCode" Type="Str">{4A8BDBBB-DA1C-45C9-8563-74C034FBD357}</Property>
				<Property Name="DistPart[0].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[7].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2014</Property>
				<Property Name="DistPart[0].SoftDep[7].upgradeCode" Type="Str">{4372F3E3-5935-4012-93AB-B6710CE24920}</Property>
				<Property Name="DistPart[0].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[8].productName" Type="Str">NI VC2008MSMs</Property>
				<Property Name="DistPart[0].SoftDep[8].upgradeCode" Type="Str">{FDA3F8BB-BAA9-45D7-8DC7-22E1F5C76315}</Property>
				<Property Name="DistPart[0].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[9].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[0].SoftDep[9].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[0].SoftDepCount" Type="Int">13</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{4722F14B-8434-468D-840D-2B0CD8CBD5EA}</Property>
				<Property Name="DistPartCount" Type="Int">1</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/E/4_NIL-Projekte/3_Projekte/2015-Braun/Installers/DPAppInstaller2015.05.28</Property>
				<Property Name="INST_buildSpecName" Type="Str">DPApp Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{88F66E09-4DFF-4818-8248-5EEFDD23F098}</Property>
				<Property Name="INST_productName" Type="Str">DPApp</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.3</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14008036</Property>
				<Property Name="MSI_arpCompany" Type="Str">NIL Nachhaltige Industrielösungen GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">info@nil-gmbh.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">03909/48 399-95</Property>
				<Property Name="MSI_arpURL" Type="Str">www.nil-gmbh.de</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{73061250-CCB0-4788-A12B-BC13F827081F}</Property>
				<Property Name="MSI_osCheck" Type="Int">5</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{143F4550-F9C7-47FC-B13D-9220C091D81E}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{F9C6324F-5685-4112-B8BD-C90C4DF2F2A1}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{DBECFBCF-2EC6-48BE-AD49-F5AA200FD5E1}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">DPApp.exe</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{4490496D-7397-48E8-879F-46E560F8BE81}</Property>
				<Property Name="Source[0].name" Type="Str">MSElibrary.dll</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/MSELibraries/MSElibrary.dll</Property>
				<Property Name="Source[0].type" Type="Str">File</Property>
				<Property Name="Source[1].dest" Type="Str">{F9C6324F-5685-4112-B8BD-C90C4DF2F2A1}</Property>
				<Property Name="Source[1].name" Type="Str">QtCore4.dll</Property>
				<Property Name="Source[1].tag" Type="Ref">/My Computer/MSELibraries/QtCore4.dll</Property>
				<Property Name="Source[1].type" Type="Str">File</Property>
				<Property Name="Source[2].dest" Type="Str">{F9C6324F-5685-4112-B8BD-C90C4DF2F2A1}</Property>
				<Property Name="Source[2].name" Type="Str">QtXml4.dll</Property>
				<Property Name="Source[2].tag" Type="Ref">/My Computer/MSELibraries/QtXml4.dll</Property>
				<Property Name="Source[2].type" Type="Str">File</Property>
				<Property Name="Source[3].dest" Type="Str">{88F66E09-4DFF-4818-8248-5EEFDD23F098}</Property>
				<Property Name="Source[3].File[0].dest" Type="Str">{88F66E09-4DFF-4818-8248-5EEFDD23F098}</Property>
				<Property Name="Source[3].File[0].name" Type="Str">DPApp.exe</Property>
				<Property Name="Source[3].File[0].Shortcut[0].destIndex" Type="Int">2</Property>
				<Property Name="Source[3].File[0].Shortcut[0].name" Type="Str">DPApp</Property>
				<Property Name="Source[3].File[0].Shortcut[0].subDir" Type="Str">DPApp</Property>
				<Property Name="Source[3].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[3].File[0].tag" Type="Str">{4490496D-7397-48E8-879F-46E560F8BE81}</Property>
				<Property Name="Source[3].FileCount" Type="Int">1</Property>
				<Property Name="Source[3].name" Type="Str">DPApp_2015.05.28</Property>
				<Property Name="Source[3].tag" Type="Ref">/My Computer/Build Specifications/DPApp_2015.05.28</Property>
				<Property Name="Source[3].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
			</Item>
		</Item>
	</Item>
</Project>
